package ust.com.HashMapProblem;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Scanner;

import javax.naming.directory.SearchControls;

import ust.com.dao.ClientdaoImp;
import ust.com.model.Client;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
    	ClientdaoImp dimp = new ClientdaoImp();
    	System.out.println("Enter the number of clients :");
    	Scanner sc=new Scanner(System.in);
    	int num=sc.nextInt();
    	int count = 1;
    	for(int i=1;i<=num;i++) {
    		System.out.println("Enter the details of the client  "+count);
    		Client client=new Client();
    		client.setName(reader.readLine());
    		client.setEmail(reader.readLine());
    		String passportNumber =reader.readLine();
    		boolean status=dimp.insertClientDetails(passportNumber, client);
    		count++;
    			
    	}
    	Map<String, Client> cmap = dimp.getAllClientDetails();
		System.out.println("Enter the passport number of the client to be searched ");
		String serachKey=reader.readLine();
		System.out.println("Client Details");
		if(cmap.containsKey(serachKey)) {
			Client cmp = cmap.get(serachKey);
			System.out.println(cmp.getName() +"--" + cmp.getEmail()+"--"+ serachKey);
		}
		else {
			System.out.println("Employee is not found");
		}

    	
    	
    }
}

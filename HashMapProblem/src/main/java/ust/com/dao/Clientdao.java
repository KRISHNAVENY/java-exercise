package ust.com.dao;

import java.util.Map;

import  ust.com.model.Client;

public interface Clientdao {
	
	boolean insertClientDetails(String 	passportNumber,Client client);	
	
	Map<String, Client> getAllClientDetails();

}
